import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import { App } from './common';
import { LoginContainer } from './login/containers';

const renderRoutes = () => (
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={LoginContainer}/>
        </Route>
    </Router>
);

export default renderRoutes;